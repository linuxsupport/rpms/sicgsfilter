Summary: Canon CQue cups foomatic filter
Name: sicgsfilter
Version: 2.0.6
Release: 4%{dist}
License: Commercial
Group: System Environment/Libraries
URL: http://it.software.canon-europe.com/software/0044596.asp

Autoreq: 0

Source0: %{name}-%{version}.tgz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%define _build_id_links none
%define debug_package %{nil}

%description
Canon CQue cups foomatic-rip filter for all
iR Advance series printers.

http://it.software.canon-europe.com/software/0044596.asp

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}

install -d -m 755 %{buildroot}/%{_bindir}

%ifarch x86_64
install -m 755 sicgsfilter.64 %{buildroot}/%{_bindir}/sicgsfilter
%else
install -m 755 sicgsfilter.32 %{buildroot}/%{_bindir}/sicgsfilter
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%{_bindir}/sicgsfilter
%doc README

%changelog
* Tue Jan 26 2021 Daniel Juarez <djuarezg@cern.ch> - 2.0.6-4
- Fix dependencies for aarch64

* Wed Nov 13 2019 Ben Morrice <ben.morrice@cern.ch> - 2.0.6-2
- release for el8

* Fri Feb 21 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.0.6-1
- initial release.


